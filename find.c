#include <stddef.h>

#include "find.h"

extern char path[8192];
extern unsigned long path_end;

/*
void chkerr(long res) {
    if (res < 0) {
        if (res == ENOENT) {
            print("find: `");
            print(path);
            print("': No such file or directory\n");
        } else if (res == ENOTDIR) {
            print("find: `");
            print(path);
            print("': Not a directory\n");
        } else {
            print("find: `");
            print(path);
            print("': Unknown error ");
            printint(res);
            printnl();
        }
        exit(2);
    }
}
*/

/*
#define BUFF_SIZE 1024
void browsedir() {
    long res, len, old_path_end, pos;
    char buf[BUFF_SIZE];
    fd_t fd;
    struct linux_dirent *entry;

    fd = open(path, 0);
    old_path_end = path_end;
    chkerr(fd);

    while ((res = getdents(fd, (struct linux_dirent *) buf, BUFF_SIZE)) > 0) {
        pos = 0;
        while (pos < res) {
            entry = (struct linux_dirent *) (buf + pos);
            pos += entry->d_reclen;

            if (checkname(entry->d_name)) {
                len = strcpy(path + path_end, entry->d_name);
                path_end += len;
                find();
                path_end = old_path_end;
            }
        }
    }

    if (res < 0) {
        exit(3);
    }
}
*/

/*
void find() {
    struct stat info;
    long res;

    res = lstat(path, &info);
    chkerr(res);

    print(path);
    printnl();

    if (isdir(&info)) {
        addslash();
        browsedir();
    }
}
*/

/*
int main(int argc, char *argv[]) {
    long res;

    if (argc < 2) {
        print("find: Too few arguments\n");
        exit(1);
    }

    res = strcpy(path, argv[1]);
    path_end += res;
    find();

    return 0;
}
*/
