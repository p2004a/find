#include "find.h"

/*
void print(const char * str) {
    unsigned long len;
    long num_wrote;

    len = strlen(str);

    while (len != 0) {
        num_wrote = write(1, str, len);
        if (num_wrote < 0) {
            goto error;
        }
        len -= num_wrote;
        str += num_wrote;
    }

    return;
error:
    exit(2);
}
*/

/*
void printnl() {
    print("\n");
}
*/

void printint(long n) {
    char out[30];
    int pos = 30;
    int negative = 0;

    if (n < 0) {
        negative = 1;
        n = -n;
    }

    out[--pos] = 0;
    do {
        out[--pos] = n % 10 + '0';
        n /= 10;
    } while (n != 0);

    if (negative) {
        out[--pos] = '-';
    }

    print(out + pos);
}