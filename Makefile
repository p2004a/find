CFLAGS= -Wall -Wextra -ansi -O0 -g -fno-builtin -fno-stack-protector -ffreestanding -march=x86-64
ASM=nasm
CC=gcc

find: find.o find_asm.o utils.o
	ld $^ -o find

find_asm.o: find.asm
	$(ASM) -g find.asm -felf64 -o find_asm.o

utils.o: utils.c
	$(CC) -c $(CFLAGS) utils.c -o utils.o

find.o: find.c
	$(CC) -c $(CFLAGS) find.c -o find.o

.PHONY: clean

clean:
	rm -f *.o find
