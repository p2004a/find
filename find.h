#include <sys/types.h>
#include <sys/stat.h>

struct linux_dirent {
   unsigned long  d_ino;     /* Inode number */
   unsigned long  d_off;     /* Offset to next linux_dirent */
   unsigned short d_reclen;  /* Length of this linux_dirent */
   char           d_name[];  /* Filename (null-terminated) */
                     /* length is actually (d_reclen - 2 -
                        offsetof(struct linux_dirent, d_name)) */
    /*
   char           pad;       // Zero padding byte
   char           d_type;    // File type (only since Linux
                             // 2.6.4); offset is (d_reclen - 1)
                             */
};

#define O_RDONLY        00000000
#define O_WRONLY        00000001
#define O_RDWR          00000002

unsigned long strlen(const char *);
long strcpy(char *dest, const char* src);
void exit(long status);

typedef unsigned long fd_t;

long write(unsigned long fd, const char *str, unsigned long len);
fd_t open(const char *path, int flags);
long close(fd_t fd);
long getdents(fd_t fd, struct linux_dirent *dirp, unsigned long count);
long lstat(const char *path, struct stat *buf);

long isdir(struct stat *info);
void addslash();
void chkerr(long res);
void browsedir();
void find();
long checkname(const char *name);

/* utils */
void print(const char * str);
void printnl();
void printint(long n);

#define ENOTDIR   -20  /* Not a directory */
#define ENOENT     -2  /* No such file or directory */

#define S_IFDIR 0x4000
