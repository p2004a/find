Find
====

Simple clone of find unix tool written in x86-64 Assembly.

Build
-----

    make

Run
---
    
    ./find path
