BITS 64

%define STDOUT 1
%define STRUCT_STAT_SIZE 144
%define nl 10 ; new line character
%define BUFF_SIZE 1024

%define ENOTDIR -20
%define ENOENT -2

%define S_IFDIR 0x4000

SECTION .bss

GLOBAL path
path resb 8192

SECTION .data

not_enaugh_arguments db "find: Too few arguments", nl, 0
new_line db nl, 0

str_error_begin db "find: `", 0
str_error_enoent_end db "': No such file or directory", 10, 0
str_error_enotdir_end db "': Not a directory", 10, 0
str_error_unknown db "': Unknown error", 10, 0

GLOBAL path_end
path_end dq 0

SECTION .text

GLOBAL _start
_start:
    mov rdi, [rsp]
    lea rsi, [rsp + 8]
    call main

    mov rdi, rax
    call exit

GLOBAL main
main: ; (argc, argv)
    cmp rdi, 2 ; check number of argumnets
    jge .cont

    mov rdi, not_enaugh_arguments ; print error message
    call print
    mov rdi, 1
    call exit

.cont:

    mov rdi, path  ; res = strcpy(path, argv[1]);
    mov rsi, [rsi + 8]
    call strcpy

    add rax, [path_end] ; path_end += res
    mov [path_end], rax

    call find

    xor rax, rax
    ret

GLOBAL print
print: ; (str)
    push r12 ; str
    push r13 ; len

    mov r12, rdi

    call strlen
    mov r13, rax

.loop:
    mov rdi, STDOUT
    mov rsi, r12
    mov rdx, r13
    call write

    cmp rax, 0
    jl .error

    sub r13, rax
    add r12, rax

    cmp r13, 0
    jg .loop

    pop r13
    pop r12
    ret

.error:
    mov rdi, 2
    call exit


GLOBAL printnl
printnl:
    mov rdi, new_line
    call print
    ret

GLOBAL chkerr
chkerr:
    push rbx
    sub rsp, 8

    mov rbx, rdi

    cmp rbx, 0
    jge .end

    mov rdi, str_error_begin
    call print

    mov rdi, path
    call print

.case_ENOENT:
    cmp rbx, ENOENT
    jne .case_ENOTDIR

    mov rdi, str_error_enoent_end
    call print

    jmp .error_end

.case_ENOTDIR:
    cmp rbx, ENOTDIR
    jne .case_default

    mov rdi, str_error_enotdir_end
    call print

    jmp .error_end

.case_default:
    mov rdi, str_error_unknown
    call print

.error_end:
    mov rdi, 2
    call exit

.end:
    add rsp, 8
    pop rbx
    ret

GLOBAL find
find:
    push rbp
    mov rbp, rsp
    sub rsp, STRUCT_STAT_SIZE + 8

    mov rdi, path
    lea rsi, [rbp - (STRUCT_STAT_SIZE + 8)]
    call lstat

    mov rdi, rax
    call chkerr

    mov rdi, path
    call print

    call printnl

    lea rdi, [rbp - (STRUCT_STAT_SIZE + 8)]
    call isdir

    test rax, rax
    jz .end

    call addslash

    call browsedir

.end:
    mov rsp, rbp
    pop rbp
    ret

GLOBAL browsedir
browsedir:
    push r13 ; pos
    push r14 ; res
    push r15 ; old_path_end
    push rbx ; fd
    push rbp
    mov rbp, rsp
    sub rsp, (BUFF_SIZE + 8)

    mov rdi, path
    xor rsi, rsi
    call open

    mov rbx, rax

    mov rdi, rax
    call chkerr

    mov r15, [path_end]

.getdents_loop:
    mov rdi, rbx
    lea rsi, [rbp - (BUFF_SIZE + 8)]
    mov rdx, (BUFF_SIZE + 8)
    call getdents

    mov r14, rax

    cmp r14, 0
    jle .check_res

    xor r13, r13 ; pos = 0
.entries_loop:
    cmp r13, r14
    jge .getdents_loop

    lea rdi, [rbp + r13 - (BUFF_SIZE + 8) + 18]
    call checkname

    test rax, rax
    jz .entries_loop_end

    lea rdi, [path + r15]
    lea rsi, [rbp + r13 - (BUFF_SIZE + 8) + 18]
    call strcpy

    lea rax, [r15 + rax]
    mov [path_end], rax

    call find

    mov [path_end], r15

.entries_loop_end:
    add r13w, [rbp + r13 - (BUFF_SIZE + 8) + 16]

    jmp .entries_loop

.check_res:
    cmp r14, 0
    jge .end

    mov rdi, 3
    call exit

.end:
    mov rsp, rbp
    pop rbp
    pop rbx
    pop r15
    pop r14
    pop r13
    ret

; for names .. and . returns false
GLOBAL checkname
checkname:
    xor rax, rax

    cmp BYTE [rdi], '.'
    jne .end_true

    cmp BYTE [rdi + 1], 0
    je .end_false

    cmp BYTE [rdi + 1], '.'
    jne .end_true

    cmp BYTE [rdi + 2], 0
    je .end_false

.end_true:
    inc rax

.end_false:
    ret

GLOBAL addslash:
addslash:
    mov rcx, [path_end]
    mov rsi, path
    mov al, [rsi + rcx - 1]
    cmp al, '/'
    je .end

    mov [rsi + rcx], BYTE '/'
    inc rcx
    mov [rsi + rcx], BYTE 0
    mov [path_end], rcx

.end:
    ret

GLOBAL isdir
isdir:
    ; rdi: struct stat info
    xor rax, rax
    test DWORD [rdi + 24], S_IFDIR
    jz .end
    inc rax
.end:
    ret

GLOBAL strlen
strlen:
    ; rdi string
    mov rsi, rdi
    xor rax, rax
    xor rcx, rcx
    not rcx
    repne scasb
    not rcx
    dec rcx
    mov rax, rcx
    ret

; doing strcpy from rsi to rdi and counting number of copied bytes
GLOBAL strcpy
strcpy:
    mov rdx, rsi
.loop:
    lodsb
    stosb
    test al, al
    jne .loop

    lea rax, [rsi - 1]
    sub rax, rdx
    ret

GLOBAL write
write:
    mov rax, 1
    syscall
    ret

GLOBAL exit
exit:
    mov rax, 60
    syscall

GLOBAL open
open:
    mov rax, 2
    syscall
    ret

GLOBAL close
close:
    mov rax, 3
    syscall
    ret

GLOBAL getdents
getdents:
    mov rax, 78
    syscall
    ret

GLOBAL lstat
lstat:
    mov rax, 6
    syscall
    ret
